Screen Information
==================

Screen Info it's a tool for all Android developers, this project provide to the users about Screen information.


Developed By
============

* Txus Ballesteros - @DesAndrOId, <txus.ballesteros@mobandme.com>


License (LGPL Lesser General Public License)
=======

   Copyright Mob&Me 2013 (@MobAndMe)

   Licensed under the GPL Lesser General Public License, Version 3.0 (the "License"),  
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.gnu.org/licenses/gpl.html

   Unless required by applicable law or agreed to in writing, software 
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
