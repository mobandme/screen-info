/**
   Copyright Mob&Me 2013 (@MobAndMe)

   Licensed under the GPL General Public License, Version 3.0 (the "License"),  
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.gnu.org/licenses/gpl.html

   Unless required by applicable law or agreed to in writing, software 
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
   Website: http://mobandme.com
   Contact: Txus Ballesteros <txus.ballesteros@mobandme.com>
*/

package com.mobandme.screeninfo.helpers;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;

public class ScreenHelper {

	public static DisplayMetrics getScreenInfo(Context pContext) {
		DisplayMetrics screenMetrics = null;
		
		if (pContext != null && pContext instanceof Activity) {
			Display screen = ((Activity)pContext).getWindowManager().getDefaultDisplay();
			
			if (screen != null) {
				screenMetrics = new DisplayMetrics();
				screen.getMetrics(screenMetrics);
			}
		}
		
		return screenMetrics;
	}
}
