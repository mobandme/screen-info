/**
   Copyright Mob&Me 2013 (@MobAndMe)

   Licensed under the GPL General Public License, Version 3.0 (the "License"),  
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.gnu.org/licenses/gpl.html

   Unless required by applicable law or agreed to in writing, software 
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
   Website: http://mobandme.com
   Contact: Txus Ballesteros <txus.ballesteros@mobandme.com>
*/

package com.mobandme.screeninfo;

import com.mobandme.screeninfo.helpers.ScreenHelper;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		try {
			
			initializeActivity();
			
		} catch (Exception e) {
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void initializeActivity() {
		setContentView(R.layout.activity_main);
		
		DisplayMetrics metrics = ScreenHelper.getScreenInfo(this);
		if (metrics != null) {
			
			((TextView)findViewById(R.id.ScreenInfo_Width)).setText(getString(R.string.screen_info_width_value, metrics.widthPixels));
			((TextView)findViewById(R.id.ScreenInfo_Height)).setText(getString(R.string.screen_info_height_value, metrics.heightPixels));
			((TextView)findViewById(R.id.ScreenInfo_Density)).setText(getString(R.string.screen_info_density_value, metrics.densityDpi));
			((TextView)findViewById(R.id.ScreenInfo_DensityFactor)).setText(getString(R.string.screen_info_density_factor_value, metrics.density));
			((TextView)findViewById(R.id.ScreenInfo_ScaledDensity)).setText(getString(R.string.screen_info_scaled_density_factor_value, metrics.scaledDensity));
			
		} else {
			findViewById(R.id.ScreenInfo_Layout).setVisibility(View.GONE);
		}
	}
}
